# frozen_string_literal: true

require 'spec_helper'
require_relative '../app/recipes'
require_relative './factories'

set :views, File.dirname(__FILE__) + '/../app/views'

# rubocop:disable Metrics/BlockLength
RSpec.describe 'App' do
  def app
    Sinatra::Application
  end

  let(:contentful_client) { instance_spy(Contentful::Client) }

  before do
    allow(Contentful::Client).to(receive(:new).with(
      space: 'space_id',
      access_token: 'access_token'
    ).and_return(contentful_client))
  end

  describe 'GET /recipes' do
    it 'displays recipes list' do
      contentful_recipes = Array.new(rand(1..10), Factories.contentful_recipe)
      allow(contentful_client).to(receive(:entries).and_return(contentful_recipes))

      get '/recipes'

      contentful_recipes.each do |recipe|
        expect(last_response.body).to include(recipe.title)
        expect(last_response.body).to include(recipe.photo.url)
      end
    end
  end

  describe 'GET /recipe/:recipe_id' do
    it 'displays recipe details' do
      contentful_recipe = Factories.contentful_recipe
      allow(contentful_client).to(receive(:entry).and_return(contentful_recipe))

      get "/recipes/#{contentful_recipe.id}"

      response = last_response.body

      expect(response).to include(contentful_recipe.title)
      expect(response).to include(contentful_recipe.photo.url)
      expect(response).to include(contentful_recipe.description)
      expect(response).to include(contentful_recipe.chef.name)

      contentful_recipe.tags.each do |tag|
        expect(last_response.body).to include(tag.name)
      end
    end

    it 'displays recipe not found page when recipe is not found' do
      allow(contentful_client).to(receive(:entry).and_return(nil))

      get '/recipes/invalid_recipe'

      response = last_response.body

      expect(response).to include('Recipe could not be found!')
    end
  end

  it 'displays not found page when route is invalid' do
    get '/invalid_page'

    response = last_response.body

    expect(response).to include('Page not found!')
  end
end
# rubocop:enable Metrics/BlockLength
