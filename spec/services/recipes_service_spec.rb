# frozen_string_literal: true

require 'spec_helper'
require 'contentful'
require_relative '../../app/services/recipes_service'
require_relative '../factories'

# rubocop:disable Metrics/BlockLength
RSpec.describe RecipesService do
  let(:contentful_client) { instance_spy(Contentful::Client) }

  before do
    allow(Contentful::Client).to(receive(:new).with(
      space: 'space_id',
      access_token: 'access_token'
    ).and_return(contentful_client))
  end

  describe 'list' do
    it 'lists all recipes' do
      contentful_recipes = Array.new(rand(1..10), Factories.contentful_recipe)
      allow(contentful_client).to(receive(:entries).and_return(contentful_recipes))

      recipes_list = described_class.list_recipes

      expect_contentful_client_to_be_called
      expect(contentful_client).to(have_received(:entries).with(content_type: 'recipe'))

      recipes_list.each_with_index do |recipe, index|
        contentful_recipe = contentful_recipes[index]

        expect(recipe.id).to eq(contentful_recipe.id)
        expect(recipe.title).to eq(contentful_recipe.title)
        expect(recipe.image_url).to eq(contentful_recipe.photo.url)
        expect(recipe.description).to eq(contentful_recipe.description)
        expect(recipe.chef_name).to eq(contentful_recipe.chef.name)
        expect(recipe.tags).to eq(contentful_recipe.tags.map(&:name))
      end
    end

    it 'returns empty list when there are no recipes' do
      allow(contentful_client).to(receive(:entries).and_return([]))

      expect(described_class.list_recipes.size).to equal(0)

      expect_contentful_client_to_be_called
      expect(contentful_client).to(have_received(:entries).with(content_type: 'recipe'))
    end
  end

  describe 'fetch_recipe' do
    it 'fetches one recipe entry' do
      contentful_recipe = Factories.contentful_recipe
      allow(contentful_client).to(receive(:entry).and_return(contentful_recipe))

      recipe = described_class.fetch_recipe(contentful_recipe.id)

      expect_contentful_client_to_be_called
      expect(contentful_client).to(have_received(:entry).with(contentful_recipe.id))

      expect(recipe.id).to equal(contentful_recipe.id)
      expect(recipe.title).to equal(contentful_recipe.title)
      expect(recipe.image_url).to equal(contentful_recipe.photo.url)
      expect(recipe.description).to equal(contentful_recipe.description)
      expect(recipe.chef_name).to equal(contentful_recipe.chef.name)
      expect(recipe.tags).to eq(contentful_recipe.tags.map(&:name))
    end

    it 'returns nil when the entry found is not a recipe' do
      not_recipe_entry = Factories.contentful_entry
      allow(contentful_client)
        .to(receive(:entry)
        .and_return(not_recipe_entry))

      expect(described_class.fetch_recipe(not_recipe_entry.id)).to be_nil

      expect_contentful_client_to_be_called
      expect(contentful_client).to(have_received(:entry).with(not_recipe_entry.id))
    end

    it 'return nil when recipe is not found' do
      allow(contentful_client)
        .to(receive(:entry)
        .and_return(nil))

      expect(described_class.fetch_recipe('someid')).to be_nil

      expect_contentful_client_to_be_called
      expect(contentful_client).to(have_received(:entry).with('someid'))
    end
  end

  def expect_contentful_client_to_be_called
    expect(Contentful::Client)
      .to(have_received(:new)
      .with(space: 'space_id', access_token: 'access_token'))
  end
end
# rubocop:enable Metrics/BlockLength
