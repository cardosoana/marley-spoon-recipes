# frozen_string_literal: true

require 'ostruct'

module Factories
  def self.contentful_entry
    OpenStruct.new(
      id: Faker::Alphanumeric.alphanumeric(number: 10),
      content_type: contentful_content_type('not_a_recipe')
    )
  end

  def self.contentful_recipe
    OpenStruct.new(
      id: Faker::Alphanumeric.alphanumeric(number: 10),
      title: Faker::Food.dish,
      description: Faker::Food.description,
      photo: contentful_photo,
      chef: contentful_chef,
      content_type: contentful_content_type('recipe'),
      tags: Array.new(rand(0..2), Factories.contentful_tags)
    )
  end

  def self.contentful_photo
    OpenStruct.new(url: Faker::Internet.url(path: '/photo.jpg'))
  end

  def self.contentful_chef
    OpenStruct.new(name: Faker::Name.name)
  end

  def self.contentful_content_type(type)
    OpenStruct.new(id: type)
  end

  def self.contentful_tags
    OpenStruct.new(name: Faker::Commerce.department(max: 1))
  end
end
