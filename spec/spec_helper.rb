# frozen_string_literal: true

ENV['RACK_ENV'] = 'test'

require('./config/environment')

require 'dotenv'
Dotenv.load('.env.test')

RSpec.configure do |config|
  config.include Rack::Test::Methods

  config.order = :random
end
