# frozen_string_literal: true

require 'spec_helper'
require_relative '../../app/models/recipe'
require_relative '../factories'

# rubocop:disable Metrics/BlockLength
RSpec.describe Recipe do
  it 'is created base on a contentful recipe' do
    contentful_recipe = Factories.contentful_recipe

    recipe = described_class.new(contentful_recipe)

    expect(recipe.id).to equal(contentful_recipe.id)
    expect(recipe.title).to equal(contentful_recipe.title)
    expect(recipe.image_url).to equal(contentful_recipe.photo.url)
    expect(recipe.description).to equal(contentful_recipe.description)
    expect(recipe.chef_name).to equal(contentful_recipe.chef.name)
    expect(recipe.tags).to eq(contentful_recipe.tags.map(&:name))
  end

  it 'is valid even when contentful recipe does not contain tags' do
    contentful_recipe = Factories.contentful_recipe
    contentful_recipe.delete_field(:tags)

    recipe = described_class.new(contentful_recipe)

    expect(recipe.id).to equal(contentful_recipe.id)
    expect(recipe.title).to equal(contentful_recipe.title)
    expect(recipe.image_url).to equal(contentful_recipe.photo.url)
    expect(recipe.description).to equal(contentful_recipe.description)
    expect(recipe.chef_name).to equal(contentful_recipe.chef.name)
    expect(recipe.tags).to eq([])
  end

  it 'is valid even when contentful recipe does not contain chef' do
    contentful_recipe = Factories.contentful_recipe
    contentful_recipe.delete_field(:chef)

    recipe = described_class.new(contentful_recipe)

    expect(recipe.id).to equal(contentful_recipe.id)
    expect(recipe.title).to equal(contentful_recipe.title)
    expect(recipe.image_url).to equal(contentful_recipe.photo.url)
    expect(recipe.description).to equal(contentful_recipe.description)
    expect(recipe.chef_name).to equal('')
    expect(recipe.tags).to eq(contentful_recipe.tags.map(&:name))
  end
end
# rubocop:enable Metrics/BlockLength
