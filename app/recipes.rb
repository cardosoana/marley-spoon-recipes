# frozen_string_literal: true

require 'sinatra'
require_relative './services/recipes_service'

get '/' do
  redirect to('/recipes')
end

get '/recipes' do
  @recipes = RecipesService.list_recipes
  erb :index
end

get '/recipes/:id' do
  @recipe = RecipesService.fetch_recipe(params[:id])

  if @recipe
    erb :show
  else
    erb :not_found
  end
end

error Sinatra::NotFound do
  erb :page_not_found
end
