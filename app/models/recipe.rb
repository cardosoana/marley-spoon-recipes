# frozen_string_literal: true

class Recipe
  attr_reader :id, :title, :image_url, :description, :tags, :chef_name

  def initialize(contentful_recipe)
    @id = contentful_recipe.id
    @title = contentful_recipe.title
    @image_url = contentful_recipe.photo.url
    @description = contentful_recipe.description

    @chef_name = ''
    @chef_name = contentful_recipe.chef.name if contentful_recipe.respond_to?(:chef)

    @tags = []
    @tags = contentful_recipe.tags.map(&:name) if contentful_recipe.respond_to?(:tags)
  end
end
