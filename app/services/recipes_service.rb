# frozen_string_literal: true

require 'contentful'
require 'dotenv/load'

require_relative '../models/recipe'

module RecipesService
  def self.list_recipes
    client
      .entries(content_type: 'recipe')
      .map { |entry| Recipe.new(entry) }
  end

  def self.fetch_recipe(id)
    entry = client.entry(id)

    Recipe.new(entry) if entry && entry.content_type.id == 'recipe'
  end

  private_class_method def self.client
    Contentful::Client.new(
      space: ENV['CONTENTFUL_SPACE_KEY'],
      access_token: ENV['CONTENTFUL_ACCESS_TOKEN']
    )
  end
end
