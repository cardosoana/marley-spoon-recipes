install:
	gem install bundler
	bundle install

start:
	ruby app/recipes.rb

test:
	bundle exec rspec
