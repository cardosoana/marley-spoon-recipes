# Marley Spoon Web Challenge

This is my solution to the Marley Spoon code challenge.
Briefly, the challenge is to implement an application to consume Recipes data from an API hosted on Contentful and display it.

## Dependencies

I've decided to use [Sinatra](https://github.com/sinatra/sinatra) because it is a lightweight and low effort web framework. For the tests, I choose [Rspec](https://github.com/rspec/rspec-rails).

I'm also using:
- [Contentful Ruby SDK](https://github.com/contentful/contentful.rb) to consume the Contentful API
- [Faker](https://github.com/faker-ruby/faker) to generate fake data for my tests
- [dotenv](https://github.com/bkeepers/dotenv) to load environment variables

## Project Structure

I've created a **RecipesService** module (`app/services/recipes_service.rb`) to be responsible for consuming Contentful API data. The **Recipe** model (`app/models/recipe.rb`) is the representation of a recipe object (I didn't want to couple the internal code of my application with the implementation of Contentful objects). The `recipes.rb` file is responsible for defining the routes and controlling the application flow.

The `app/views` folder contains the `erb` templates and the `CSS` file is inside `app/public` folder.  

## User Interface

About the user interface:
- I used `@media` rule to have a more responsive design and improve the experience of mobile device users
- The Marley Spoon web page was the reference for the color palette.
- I used [WAVE - Web Accessibility Evaluation Tool](https://wave.webaim.org/) to help me to build a more accessible content.

### Recipes List
![screenshot of recipes list page](./README/images/recipes_list.png)

### Recipe Details
![screenshot of recipes list page](./README/images/recipe_details.png)

## Running the application

Install project dependencies

```
make install
```

Make a copy of `.env.example` to `.env` and configure your environment variables

```
cp .env.example .env
```

Run the application

```
make start
```

The application will be available on http://localhost:4567.

## Running the tests

```
make test
```
